package Func

import (
	"fmt"
	"testing"
)

// 参数可选, 函数式选项模式


type Config struct {
	Zhang3 string
	Li4 string
	Wang5 string
}

type SetOption func(o *Config)

func WithZhang(name string) SetOption{
	return func(o *Config) {
		o.Zhang3 = name
	}
}

func WithLi(name string) SetOption{
	return func(o *Config) {
		o.Li4 = name
	}
}

func WithWang(name string) SetOption{
	return func(o *Config) {
		o.Wang5 = name
	}
}
// WithWang 等是作为参数来使用的;
// name和顺序: 要注意,在使用上肯定是name先传递了,然后o是二次传递

func NewConfig(opts ...SetOption)  *Config{
	oo := Config{}
	for _, op := range opts{
		op(&oo)
	}
	return &oo
}

func TestRun(t *testing.T) {
	oo := NewConfig(
		WithZhang("zhang100"),
		WithLi("Li100"),
		WithWang("Wang100"),
		)

	fmt.Println(*oo)
}
