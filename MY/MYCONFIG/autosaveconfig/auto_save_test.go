package autosaveconfig

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"testing"
	"time"
)

var Conf *viper.Viper

func initConfig() {
	fmt.Println("initConfig...")
	Conf = viper.New()
	Conf.SetConfigName("ddj")
	Conf.SetConfigType("json")
	Conf.AddConfigPath(".")
	Conf.ReadInConfig()
	// 打印名字
	fmt.Println(Conf.GetString("name"))

	Conf.WatchConfig()
	go Conf.OnConfigChange(func(in fsnotify.Event) {
		fmt.Println("in name: ", in.Name)
		// 重新读配置文件
		Conf.ReadInConfig()
		// 名字更改后,查看更改后的名字
		fmt.Println(Conf.GetString("name"))
	})
	fmt.Println("Sleep num Second...")
	time.Sleep(time.Second * 9)
}

func TestRun(t *testing.T) {
	initConfig()
}
