package manyconfig

import (
	"fmt"
	"github.com/spf13/viper"
	"testing"
)

type MyInt int

func (i MyInt) Print() {
	fmt.Println("MyInt value: ", i)
}

func (i MyInt) Add(other MyInt) int {
	return int(i) + int(other)
}

func TestOtherBase(t *testing.T) {
	a := 12
	b := float64(a)
	fmt.Println("b", b)
	var c MyInt = 2
	c.Print()
	fmt.Println(c.Add(MyInt(23)))
}

type MyViper struct {
	vp *viper.Viper
}

func (self *MyViper) Init(name, typ, path string) {
	self.vp.SetConfigName(name)
	self.vp.SetConfigType(typ)
	self.vp.AddConfigPath(path)
	self.vp.ReadInConfig()
}

func (self *MyViper) Print() {
	allSettings := self.vp.AllSettings()
	fmt.Println("-------allSettings------")
	for k, v := range allSettings {
		fmt.Println(k, " : ", v)
	}
}

func (self *MyViper) SetDefaultSelf() {
	fmt.Println("___default self")
	allSettings := self.vp.AllSettings()
	for k, v := range allSettings {
		self.vp.SetDefault(k, v)
	}
}

var otherViper MyViper

func initConfig() {
	otherViper.vp = viper.New()

	otherViper.Init("ddy", "yaml", ".")
	otherViper.Print()
	otherViper.SetDefaultSelf()
	otherViper.Init("ddj", "json", ".")
	otherViper.Print()
}
func TestManyConfig(t *testing.T) {
	initConfig()
}
