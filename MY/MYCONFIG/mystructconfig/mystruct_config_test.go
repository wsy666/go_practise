package mystructconfig

import (
	"fmt"
	"github.com/spf13/viper"
	"testing"
)

var Conf *viper.Viper

func TestRun(t *testing.T) {
	initConfig()
	var pper = &Person{}
	fmt.Println("DumpToPerson...")
	DumpToPerson(pper)
	pper.Print()

	pper.Name = "Li4"
	// todo 如何将更改的姓名同步到配置文件并保存
}

type Person struct {
	Name string `mapstructure:"name"`
	Age  int    `mapstructure:"age"`
}

func (receiver Person) Print() {
	fmt.Printf("<Person %s, %d>\n", receiver.Name, receiver.Age)
}

func initConfig() {
	fmt.Println("initConfig...")
	Conf = viper.New()
	Conf.SetConfigName("ddj")
	Conf.SetConfigType("json")
	Conf.AddConfigPath(".")
	Conf.ReadInConfig()
}

// marshal
func DumpToPerson(per *Person) {
	Conf.Unmarshal(per)
}
