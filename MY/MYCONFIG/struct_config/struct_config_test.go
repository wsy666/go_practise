package struct_config

import (
	"flag"
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"testing"
	//"net/http"
	//"net/http"
)

// 所有的配置信息;
// 注意 viper使用的第三方库,当我们打tag时,无论什么类型的文件都打mapstructure
type Config struct {
	Port        int    `mapstructure:"port"`
	Version     string `mapstructure:"version"`
	MySQLConfig `mapstructure:"mysql"`
}

type MySQLConfig struct {
	Host   string `mapstructure:"host"`
	DBName string `mapstructure:"db_name"`
	Port   int    `mapstructure:"port"`
}

// 注意这里的Conf是指针;
// 只所以是指针,是因为当配置文件变化时, 调用回调函数,更新配置到Conf中,系统
// 就能第一时间获取到新的配置信息;
var Conf = new(Config)

func TestStructConfig(t *testing.T) {
	// 设置默认值
	viper.SetDefault("fileDir", "./")

	// 使用命令行参数,内置flag实现
	configFilePath := flag.String("c", "./config.yaml", "批定配置文件的相对路径 eg:-c=./xx/xx.yaml")
	// 解析命令行参数
	flag.Parse()
	// 读取配置文件
	// 方式1 使用命令行参数的形式指定配置文件;
	viper.SetConfigFile(*configFilePath)
	//
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// 配置文件没有找到, 如果需要可以忽略这个错误
			panic(fmt.Errorf("Fatal error config file, err:%v\n", err))
		} else {
			// 配置文件找到了, 但出现了其它的错误
			panic(fmt.Errorf("Fatal error config file, err:%v\n", err))
		}
	}

	// 配置文件找到并成功解析
	if err := viper.Unmarshal(Conf); err != nil {
		panic(fmt.Errorf("unmarshal conf failed, err: %v\n", err))
	}

	viper.WatchConfig()
	viper.OnConfigChange(func(in fsnotify.Event) {
		fmt.Println("配置文件更改了")
		if err := viper.Unmarshal(Conf); err != nil {
			panic(fmt.Errorf("unmarshal conf failed, err: %v\n", err))
		}
		fmt.Println("in.Name", in.Name) // 打印配置文件名;
		fmt.Println("已同步配置")

	})

	r := gin.Default()
	r.GET("/version", func(context *gin.Context) {
		context.JSON(200, gin.H{
			"version": Conf.Version,
		})
	})

	fmt.Printf("\n\n%#v\n", Conf)
	fmt.Printf("%#v\n", Conf.MySQLConfig)

	//rParam, _ := fmt.Printf(":%d", Conf.Port)
	url := "127.0.0.1:8888"
	if err := r.Run(url); err != nil {
		panic(err)
	}

}
