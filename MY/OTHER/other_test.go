package OTHER

import (
	"fmt"
	"github.com/spf13/cast"
	"testing"
	"time"
)

const FixDateTimeForFormat = "2006-01-02 15:04:05"
func TestDate(t *testing.T) {
	// 上周的表示
	now := time.Now()
	lastWeek := now.AddDate(0, 0, -7)
	fmt.Println("当前时间", now.Format(FixDateTimeForFormat))
	fmt.Println("上周时间", lastWeek.Format(FixDateTimeForFormat))

	// 字符串解析成time
	layout := "2006-01-02"
	dateString := "2023-07-19"
	parsedTime, err := time.Parse(layout, dateString)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("字符串解析为time对象: ", parsedTime)
}

func TestSlice(t *testing.T) {
	var s[]int
	s = append(s, 10)
	fmt.Println(s)

	var m map[string]int
	m = make(map[string]int)
	m["one"] = 1
	fmt.Println(m)


}

func ModifyMap(m map[string]int) map[string]int {
	_, ok := m["aa"]
	if ok{
		delete(m, "aa")
	}else{
		m["aa"] = 100
	}
	return m
}

func ModifySlice(slice []int) []int {
	slice = append(slice, 100)
	return slice
}


func TestMoifyMapSlice(t *testing.T) {
	m := map[string]int{
		"a": 1,
		"b": 2,
	}

	slice := []int{1, 2, 3}

	mm := ModifyMap(m)
	sliceCopy := ModifySlice(slice)
	//fmt.Println(m == mm)
	//fmt.Println(slice == sliceCopy)
	fmt.Println(mm)
	fmt.Println(sliceCopy)
}

func AddInterfaceNumber(a interface{}) {
	aa, _ := a.(int)
	res := aa + 2
	fmt.Println(res)
}

func TestInterfaceInt(t *testing.T) {
	AddInterfaceNumber(3)
}

type Map map[interface{}]interface{}

func (m Map)IsEqual(other map[interface{}]interface{}) bool{

	for k, v := range other{
		if m.IsKeyExists(k){
			if v != m[k]{
				fmt.Println("compare: ", v, m[k])
				return false
			}
		}else{
			return false
		}
	}
	return true
}

func (m Map) IsKeyExists(key interface{}) bool {
	_, ok := m[key]
	if ok{
		return true
	}
	return false
}

//func (m Map) ConvertToMap(other map[interface{}]interface{}) Map {
//	return Map(other)
//}

func ConvertMapToInterfaceMap(m map[string]int) map[interface{}]interface{} {
	newInter := map[interface{}]interface{}{}
	for k, v := range m{
		var kk interface{} = k
		var vv interface{} = v
		newInter[kk] = vv
	}

	return newInter
}

func TestNewMap(t *testing.T) {
	newMap := Map{
		"a": 1,
		"b": 2,
		"c": 3,
	}
	fmt.Println(newMap.IsKeyExists("b")) // true
	fmt.Println(newMap.IsKeyExists("c")) // true
	fmt.Println(newMap.IsKeyExists("d")) // false

	other := map[string]int{
		"e": 5,
		"f": 6,
		"g": 7,
	}
	//fmt.Println(newMap.ConvertToMap(other)) error
	otherInterfaceMap := ConvertMapToInterfaceMap(other)
	fmt.Println("m = im ?", newMap.IsEqual(otherInterfaceMap))
}

/* ==================================================================================
interface int
   ==================================================================================
*/

type Myint int

func (i Myint) IsEqual(a interface{}) bool {
	if int(i) == a.(int){
		return true
	}
	return false
}

func TestInt(t *testing.T) {
	my := Myint(2)
	fmt.Println(my.IsEqual(2))
	fmt.Println(my.IsEqual(3))
}


/* ==================================================================================
interface
   ==================================================================================
*/
func TestInterfaceVariable(t *testing.T)  {
	var a interface{} = 2
	var b interface{} = 2
	fmt.Println(a == b) // true

	fmt.Println( 2 + 3 )
	//fmt.Println(a + b ) erro
	//fmt.Println(int(a) + int(b))
	fmt.Println(a.(int) + b.(int)) // 4

	fmt.Println(cast.ToInt(a) + cast.ToInt(b)) // 4

	var c interface{} = "3"
	fmt.Println(2 + cast.ToInt(c)) // 5
	//fmt.Println(2 + c.(int)) // error


}

type BlankInterface interface {}

func TestBlankInterface(t *testing.T) {
	var a BlankInterface = 2
	var b BlankInterface = 3
	fmt.Println(cast.ToInt(a) + cast.ToInt(b))

	var c interface{} = 3
	fmt.Println(b == c)  // true

	var d int = 3
	var e Myint = 3
	//fmt.Println(d == e )  wwdssdffsdafdfasadfadfasdffas
}sdf
