package models

import "gorm.io/gorm"

var db *gorm.DB

type Model struct {
	ID         int `json:"id" gorm:"primary_key"`
	CreateOn   int `json:"create_on" `
	ModifiedOn int `json:"modified_on"`
	DeletedOn  int `json:"deleted_on"`
}

//func Setup() {
//	var err error
//	db, err = gorm.Open(
//
//		)
//}
//
//func CloseDB() {
//	defer db.Close()
//}
//
//func
