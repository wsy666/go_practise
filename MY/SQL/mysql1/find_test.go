package mysql1

import (
	"fmt"
	"testing"
	"time"
)

func TestRun(t *testing.T) {
	FindA()
}

func FindA() {
	DB = GetDB()
	var vo Video
	rsOne := DB.Find(&vo, "id=?", 1)
	if rsOne.RowsAffected > 0 {
		fmt.Println("查找成功,id=1: ")
		vo.PrintSelf()
	}

	// id = 2
	// !!!每次查询必须声明一个新的对象
	fmt.Println("start id = 2 ")
	var vc Video
	rsOne = DB.Find(&vc, "id=?", 2)
	fmt.Println("rsOne.Row...:", rsOne.RowsAffected)
	vc.PrintSelf()
	if rsOne.RowsAffected > 0 {
		vo.PrintSelf()
	}

	// id = 10
	fmt.Println("start id = 3")
	var vp Video
	rsTwo := DB.Find(&vp, "id=?", 3)
	if rsTwo.RowsAffected > 0 {
		vp.PrintSelf()
	}

}

//
func TestFind2(t *testing.T) {
	db := GetDB()


	// First
	vi := Video{}
	db.First(&vi)
	vi.PrintSelf()

	// 条件查询 condition
	db.Where("id=?", 1).First(&vi)
	vi.PrintSelf()

	//
	vips := []Video{}
	db.Where("title IN?", []string{"title1", "title2", "title3"}).Find(&vips)
	fmt.Println("find len(video): ", len(vips)) // 4

	// Like
	db.Where("title LIKE?", "%2%").Find(&vips)
	fmt.Println("find len(video): ", len(vips))  //3

	// and
	db.Where("title = ? AND url = ?", "title1", "url1").First(&vi)
	fmt.Println("and: ", vi)

	// time
	lastWeek := time.Now()
	db.Where("updated_at >?", lastWeek).Find(&vips)
	fmt.Println("time >  len(video): ", len(vips)) // 4

	// between
	today := time.Now()
	db.Where("created_at BETWEEN ? AND ?", lastWeek, today).Find(&vips)
	fmt.Println("between len(video): ", len(vips)) // 4
}