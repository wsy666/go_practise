package mysql1

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"testing"
)

func TestGen(t *testing.T) {
	db, closeDb := GetDBCloseFunc()
	defer closeDb()

	rsOne := db.Create(&Video{
		Title:       "ttg1",
		Description: "wandehao1",
		URL:         "http://www.ttg.com1",
	})

	if rsOne.RowsAffected > 0 {
		fmt.Println("添加一条数据")
	}
}

func TestInsertDataDirect(t *testing.T) {
	db := GetDB()
	db.Create(&Video{ Title: "title1", Description: "descr1", URL: "url1", })
	db.Create(&Video{ Title: "title2", Description: "descr2", URL: "url2", })
	db.Create(&Video{ Title: "title3", Description: "descr3", URL: "url3", })
	db.Create(&Video{ Title: "title4", Description: "descr4", URL: "url4", })
	db.Create(&Video{ Title: "title5", Description: "descr5", URL: "url5", })
	db.Create(&Video{ Title: "title6", Description: "descr6", URL: "url6", })
}

// 向mysql添加一些video对象
func DataBase2() {
	db, err := gorm.Open(mysql.Open(GetSqlPath()), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	DBA, _ := db.DB()
	defer DBA.Close()

	// new object
	rsOne := db.Create(&Video{
		Title:       "hello",
		Description: "helloworld",
		URL:         "http://www.baidu.com",
	})
	if rsOne.RowsAffected > 0 {
		fmt.Println("创建成功")
	}

	// new 2
	rsTwo := db.Create(&Video{
		Title:       "Title2",
		Description: "mmmmmm",
		URL:         "https://wwww.nihao.com",
	})
	if rsTwo.RowsAffected > 0 {
		fmt.Println("添加成功")
	}

	// find
	var v1 Video
	db.Find(&v1, "id=?", 1)
	fmt.Println("find: ", v1)

	// find all
	var videos []Video
	db.Find(&videos)
	fmt.Println(videos)

	// modify
	v1.Title = "2222222222"
	rsSave := db.Save(v1)
	if rsSave.RowsAffected > 0 {
		fmt.Println("修改成功")
	}

	//
	db.Find(&v1, "id=?", 2)
	fmt.Println("v1", v1)

	// delete
	rsDelete := db.Delete(v1)
	if rsDelete.RowsAffected > 0 {
		fmt.Println("用户删除成功")
	}
}

