package mysql1

import (
	"fmt"
	"testing"
)



type SumFunc func(a, b int) int

func (s SumFunc) Result(a, b int) int {
	return s(a, b)
}

func TestOther(t *testing.T) {
	var f SumFunc
	f  = func(a, b int) int {
		return a + b
	}

	fmt.Println(f.Result(2,3))
}
