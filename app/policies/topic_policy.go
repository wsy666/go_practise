package policies

import (
	"go_practise/pkg/auth"

	"go_practise/app/models/article"
)

// CanModifyArticle 是否允许修改话题
func CanModifyArticle(_article article.Article) bool {
	return auth.User().ID == _article.UserID
}
