package password

import (
	"fmt"
	"testing"
)

func TestRun(t *testing.T) {
	hashStr := "$2a$14$P4an0IEFX9LADPXk/0AlPu0YLJJ72fjQhPBvC0aE3AEGTj4sKp31W"
	fmt.Println("nihao->", Hash("nihao"))
	fmt.Println("CheckHash:", CheckHash("nihao", hashStr))
	fmt.Println("isHashed:", IsHashed(hashStr)) //true
}

func TestRun1(t *testing.T) {
	// 将字符串转化为[]byte类型
	by := []byte("nihao")
	fmt.Println(by)
}
