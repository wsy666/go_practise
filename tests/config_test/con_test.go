package config_test

import (
	"fmt"
	"testing"
)
import "go_practise/pkg/config"

// 打印所有配置;
func TestConfig(t *testing.T) {
	allSet := config.Viper.AllSettings()
	fmt.Println("allSet:", allSet)
}

// 打印所有的配置
func printAllSet() {
	allSet := config.Viper.AllSettings()
	//fmt.Println("allSet: ", allSet)
	fmt.Println("=== print All Set:")
	for k, v := range allSet {
		fmt.Printf("%s = %s\n", k, v)
	}
}

// 保存config到配置文件, 如果over为true则覆盖;
func SaveViper(over bool) {
	if over {
		config.Viper.WriteConfig()
	} else {
		config.Viper.SafeWriteConfig()
		//config.Viper.WriteConfigAs("") 另存为...新的文件名
	}
}

// 测试保存配置文件, 修改配置文件后 保存;
func TestSaveConfig(t *testing.T) {
	var blank interface{}
	blank = "hellApp1111"
	config.Viper.Set("APP_NAME", blank)
	printAllSet()
	// 保存config
	SaveViper(true)
}
