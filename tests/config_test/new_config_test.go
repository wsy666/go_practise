package config

import (
	"github.com/spf13/viper"
	"testing"
	"time"
)

var Viper *viper.Viper

type Config struct {
}

type MysqlConfig struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Password string `json:"password"`
}

type AppConfig struct {
	Name string
	cr   time.Time
}

// 定义两个默认的结构体
var mysqlConfig MysqlConfig
var myAppConfig AppConfig

func Define() {
	mysqlConfig = MysqlConfig{
		Host:     "127.0.0.1",
		Port:     8888,
		Password: "123456",
	}

	myAppConfig = AppConfig{
		Name: "helloApp",
		cr:   time.Now(),
	}
}

// 新建一个配置文件;
func initConfig() {

	Viper = viper.New()
	Viper.SetConfigName("con_name")
	Viper.SetConfigType("json")
	Viper.AddConfigPath(".")

	Define()

}

func TestNewConfig(t *testing.T) {

}
