package tests

import (
	"fmt"
	"testing"
)

type AbstractApple interface {
	NamePrint()
}

type Apple1 struct {
	Name string
}

type Apple2 struct {
	Name string
}

func (receiver Apple1) NamePrint() {
	fmt.Println("Apple1:", receiver.Name)
}

func (receiver Apple2) NamePrint() {
	fmt.Println("Apple2:", receiver.Name)
}

type Apple struct {
	AbstractApple
}

func TestRun(t *testing.T) {
	newApple := Apple{}
	ap2 := Apple2{Name: "ping2"}
	newApple.AbstractApple = ap2
	newApple.NamePrint()

	// 接口断言
	ap, ok := newApple.AbstractApple.(Apple2)
	if ok {
		fmt.Println(ap)
	}
}
